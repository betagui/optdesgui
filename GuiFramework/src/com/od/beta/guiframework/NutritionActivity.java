package com.od.beta.guiframework;

import android.app.Activity;
import android.os.Bundle;

public class NutritionActivity extends Activity {
	
	int nutritionID;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		nutritionID = getIntent().getIntExtra(ObjectFragment.NUT_ID, 1);
		switch(nutritionID)
		{
			case 0:
				setContentView(R.layout.glucose_detailed);
				break;
			case 1:
				setContentView(R.layout.zinc_detailed);
				break;
			case 2:
				setContentView(R.layout.about_us_tab);
				break;
			case 3:
			case 4:
			default:
				setContentView(R.layout.results_tab);
		}
		if (savedInstanceState == null) {
			//getFragmentManager().beginTransaction()
					//.add(R.id.fragmentContainer, new PlaceholderFragment()).commit();
		}
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
