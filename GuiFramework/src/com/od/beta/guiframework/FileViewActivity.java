package com.od.beta.guiframework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ScrollView;
import android.widget.TextView;

public class FileViewActivity extends Activity {

	TextView FileViewer;
	ScrollView scroller;
	private int duration = 0;
	private final String TAG = "FileViewActivity";
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(CollectionActivity.BT_CONFIG == CollectionActivity.BT_BLE)
		{
			duration = BluetoothLeService.duration;
		}
		else
		{
			//duration = Bluetooth.duration;
		}
		String FileName = BluetoothLeService.FILENAME;
		File dir = getFilesDir();
    	File file = new File(dir, FileName);
		FileViewer = new TextView(this);
		StringBuilder builder = new StringBuilder();
		//builder.append("File size: " + file.length() + " bytes\n");
		//builder.append("Duration of transfer: " + duration + "s\n");
		int ch;
		FileInputStream fis;
		try
		{
			fis = openFileInput(FileName);
			try
			{
				while((ch = fis.read()) != -1)
				{
					builder.append((char)ch);
				}
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
		catch(FileNotFoundException e)
		{
			Log.d(TAG, "File Not Found");
		}
		String output = builder.toString();
		FileViewer.append(output);
		scroller = new ScrollView(this);
		scroller.addView(FileViewer);
		setContentView(scroller);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		FileViewer.setText("");
	}
	
}
