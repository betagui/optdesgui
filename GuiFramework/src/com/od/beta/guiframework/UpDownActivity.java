package com.od.beta.guiframework;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.survivingwithandroid.HttpClient;

public class UpDownActivity extends Activity {

	private String urlBegin = "http://";
	private String urlIP;
	private String urlEnd = ":8080/FileTransfer2/FileTransferServlet";
	private MenuItem item;
	Bitmap bmp;
	ImageView resultView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us_tab);
		final EditText editIP = (EditText) findViewById(R.id.editTextIP);
		Button btnUpDown = (Button)findViewById(R.id.button1);
		resultView = (ImageView) findViewById(R.id.resultView);
		
		btnUpDown.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				urlIP = editIP.getText().toString();
				SendHttpRequestTask t = new SendHttpRequestTask();
				String fullURL = urlBegin + urlIP + urlEnd;
				String[] params = new String[]{fullURL};
				t.execute(params);
			}
		});
	}
		
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
			getMenuInflater().inflate(R.menu.login, menu);
			item = menu.getItem(0);
			return true;
	}
		
	private class SendHttpRequestTask extends AsyncTask<String, Void, String> {	
			@Override
			protected String doInBackground(String... params) {
				String url = params[0];
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				
				//read data into baos
				//InputStream in_s = getResources().openRawResource(R.raw.data2);
				//input stream = file from bluetooth
				String filename = BluetoothLeService.FILENAME;
				File dir = getFilesDir();
				File file = new File(dir, filename);
				InputStream in_s = null;
				try {
					in_s = new FileInputStream(file);
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				byte[] b_a;
				try {
					b_a = new byte[in_s.available()];
					in_s.read(b_a);
				    baos.write(b_a);
				    in_s.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//send baos
				try {
					HttpClient client = new HttpClient(url);
					client.connectForMultipart();
					client.addFilePart("file", "data1.txt", baos.toByteArray());
					client.finishMultipart();
					byte[] data = client.getResponse2();
					bmp = client.bmp;
				}
				catch(Throwable t) {
					t.printStackTrace();
				}
				
				return null;
			}

			@Override
			protected void onPostExecute(String data) {			
				item.setActionView(null);
				resultView.setImageBitmap(bmp);
			}
		}
	
}
