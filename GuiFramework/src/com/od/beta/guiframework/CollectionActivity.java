package com.od.beta.guiframework;


import java.io.File;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;

public class CollectionActivity extends FragmentActivity {

	private static final int NUM_TABS = 3;
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	private static final String TAG  = "CollectionActivity";
	public static final int BT_CLASSIC = 1;
	public static final int BT_BLE = 2;
	//set BT_CONFIG = BT_CLASSIC for bluetooth 2.0. 
	//Set BT_CONFIG = BT_BLE for bluetooth 4.0 
	public static int BT_CONFIG = BT_BLE;
	private final boolean D = true;
	private BluetoothLeService mBluetoothLeService;
	
	
	//Manages connection to Bluetooth service
	private final ServiceConnection mServiceConnection = new ServiceConnection()
	{
		public void onServiceConnected(ComponentName componentName, IBinder service) 
		{
			 mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
			 if(D) Log.d(TAG, "Bound to BluetoothLeService");
		}

		public void onServiceDisconnected(ComponentName componentName) {
			// TODO Auto-generated method stub
			if(D) Log.d(TAG, "Unbound from BluetoothLeService");
		}
	};
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_collection);
		 if(D) Log.d(TAG, "onCreate() called");

		mPager = (ViewPager) findViewById(R.id.pager);
		mPagerAdapter = new CollectionPagerAdapter(getSupportFragmentManager());
		mPager.setOffscreenPageLimit(3);
		mPager.setAdapter(mPagerAdapter);
		mPager.setPageTransformer(true, new ZoomOutPageTransformer());
		mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
		
			
			@Override
			public void onPageSelected(int position) {
				// When changing pages, reset the action bar actions since they
				// are dependent
				// on which page is currently active. An alternative approach is
				// to have each
				// fragment expose actions itself (rather than the activity
				// exposing actions),
				// but for simplicity, the activity provides the actions in this
				// sample.
				invalidateOptionsMenu();
			}

		});

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		ActionBar.TabListener tabListener = new ActionBar.TabListener() {

			public void onTabUnselected(Tab tab, FragmentTransaction ft) {
				// hide the given tab
			}

			public void onTabSelected(Tab tab, FragmentTransaction ft) {
				// show the given tab
				mPager.setCurrentItem(tab.getPosition());
			}

			public void onTabReselected(Tab tab, FragmentTransaction ft) {
				// ignore this event?
			}
		};
		
		actionBar.addTab(actionBar.newTab().setText("Results").setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setText("Home").setTabListener(tabListener));
		actionBar.addTab(actionBar.newTab().setText("About Us").setTabListener(tabListener));

		
		Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
      
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		if(D) Log.d(TAG, "onStart() called");
		super.onStart();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		if(D)Log.d(TAG, "onResume() called"); 
		super.onResume();
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		if(D) Log.d(TAG, "onStop() called");
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(D) Log.d(TAG, "onPause() called");
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unbindService(mServiceConnection);
        mBluetoothLeService = null;
	}
	
	/*
	 *  Opens up the profile page of the user
	 *  when the edit profile button is clicked
	 */
	public void onEditProfileClicked(View v)
	{
		Intent intent = new Intent(this, ProfileActivity.class);
		startActivity(intent);
	}
	
	/*
	 * 	Opens up BLEScanActivity which scans for remote Bluetooth devices
	 */
	public void onSyncClicked(View v)
	{
		if(BT_CONFIG == BT_BLE)
		{
			if(D) Log.d(TAG, "starting BLE_NewScanActivity");
			Intent intent = new Intent(getApplicationContext(), BLE_NewScanActivity.class);
			startActivity(intent);
		}
		else
		{
			Log.d(TAG, "Bluetooth 2.0 not yet implemented");
		}
	}
	
	//Opens up a file reader to view transferred files
	public void onViewFileClicked(View v)
	{
		Intent intent = new Intent(getApplicationContext(), FileViewActivity.class);
		startActivity(intent);
	}
	
	//Manual desync from remote bluetooth device
	public void onDeSyncClicked(View v)
	{
		Log.d(TAG, "DeSync Pressed");
		mBluetoothLeService.disconnect();
	}
	
	//Manual Deletion of File from internal memory
	public void onDeleteFileClicked(View v)
	{
		File dir = getFilesDir();
    	File file = new File(dir, BluetoothLeService.FILENAME);
    	file.delete();
	}
	
	/** Creates the 3 pages for the 3 tabs created and also gives each page
	 * an identifier-like argument that will be passed to ObjectFragment to
	 * identify one page from another
	 */
	public class CollectionPagerAdapter extends FragmentStatePagerAdapter{
		//private Context mContext;
		public CollectionPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int i) {
			Fragment fragment = new ObjectFragment(getFilesDir());
			Bundle args = new Bundle();
			if (i == 0) {
				args.putString(ObjectFragment.ARG_OBJECT, "Results Tab");
			} else if (i == 1) {
				args.putString(ObjectFragment.ARG_OBJECT, "Home Tab");
			} else if (i == 2) {
				args.putString(ObjectFragment.ARG_OBJECT, "About Tab");
			}
			
			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			return NUM_TABS;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "";
		}
	}		
}
