package com.od.beta.guiframework;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView.LegendAlign;
import com.jjoe64.graphview.GraphViewDataInterface;
import com.jjoe64.graphview.GraphViewSeries;
import com.jjoe64.graphview.GraphViewSeries.GraphViewSeriesStyle;
import com.jjoe64.graphview.LineGraphView;



/*
 * This Class serves up the xml sheets to the ViewPager in CollectionActivty based on the 
 * arguments passed in by the pager adapter
 */
public class ObjectFragment extends Fragment {

	public static final String ARG_OBJECT = "object";
	public static final String TAG = "ObjectFragment";
	public static final String NUT_ID = "nutrition_id";
	public static final String KEY_BMP = "key_bmp";
	//private Context mContext;
	private  ArrayList <String> mNutElements;
	private  NutritionAdapter mAdapter;
	private File FileDir;
	
	String urlBegin = "http://";
	String urlIP;
	String urlEnd = ":8080/FileTransfer2/FileTransferServlet";
	MenuItem item;
	Bitmap bmp;
	ImageView resultView;
	
	public ObjectFragment()
	{
		mNutElements = new ArrayList<String>();
		mNutElements.add("Glucose");
		mNutElements.add("Zinc");
		mNutElements.add("Iron");
		mNutElements.add("Calcium");
		mNutElements.add("Sodium");
	}
	
	public ObjectFragment(File dir)
	{
		mNutElements = new ArrayList<String>();
		mNutElements.add("Glucose");
		mNutElements.add("Zinc");
		mNutElements.add("Iron");
		mNutElements.add("Calcium");
		mNutElements.add("Sodium");
		FileDir = dir;
	}
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState){
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putParcelable(KEY_BMP, bmp);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View rootView;
		Bundle args = getArguments();
		
		if(args.getString(ARG_OBJECT).equals("Results Tab")){
			rootView = inflater.inflate(R.layout.results_tab, container, false);
			
			GraphViewSeries exampleSeries = new GraphViewSeries("Sample Curve", new GraphViewSeriesStyle(Color.rgb(90, 250, 00), 3), new GraphViewData[] {
				new GraphViewData(0,0d)
				, new GraphViewData(1, 4.0d)
	            , new GraphViewData(3, 2d)
	            , new GraphViewData(6, 9d)
	            , new GraphViewData(9, 6d)
	        });
		GraphViewSeries exampleSeries2 = new GraphViewSeries("Sample Curve 2", new GraphViewSeriesStyle(Color.rgb(200, 50, 00), 3), new GraphViewData[] {
			new GraphViewData(0,0d)
			, new GraphViewData(2, 8.0d)
            , new GraphViewData(6, 4d)
            , new GraphViewData(12, 18d)
            , new GraphViewData(18, 12d)
        });
		
		LineGraphView graphView = new LineGraphView(getActivity().getApplicationContext(), "Graph Demo"); //Fragment has no context, need getApplicationContext to get actual context
        graphView.addSeries(exampleSeries); // data
        graphView.addSeries(exampleSeries2);
        //graphView.getGraphViewStyle().setGridColor(getResources().getColor(R.color.turqoise));
        graphView.getGraphViewStyle().setHorizontalLabelsColor(Color.GRAY);
        graphView.getGraphViewStyle().setVerticalLabelsColor(getResources().getColor(R.color.turqoise));
        graphView.getGraphViewStyle().setNumHorizontalLabels(10);
        graphView.getGraphViewStyle().setNumVerticalLabels(10);
        //graphView.getGraphViewStyle().setVerticalLabelsWidth(300);
        
        // set legend
        graphView.setShowLegend(true);
        graphView.setLegendAlign(LegendAlign.BOTTOM);
        graphView.setLegendWidth(275);
        
        //graphView.setBackgroundColor(Color.BLUE);
        

        LinearLayout layout = (LinearLayout) rootView.findViewById(R.id.graph1); // need to use rootview to associate the findVIewById to the results tab
        layout.addView(graphView);
			
			
			
			Log.d(TAG, "inflating results tab");
		}
		else if(args.getString(ARG_OBJECT).equals("Home Tab")){
			rootView = inflater.inflate(R.layout.home_tab, container, false);
			ListView nutList = (ListView) rootView.findViewById(R.id.nutrition_list);
			mAdapter = new NutritionAdapter(mNutElements);
			
			nutList.setAdapter(mAdapter);
			
		}
		else{	//About Page
			rootView = inflater.inflate(R.layout.about_us_tab, container, false);
			if(savedInstanceState != null){
				bmp = savedInstanceState.getParcelable(KEY_BMP);
			}
			// Server/Client Code starts here
			
			final EditText editIP = (EditText) rootView.findViewById(R.id.editTextIP);
			Button btnUpDown = (Button)rootView.findViewById(R.id.button1);
			resultView = (ImageView) rootView.findViewById(R.id.resultView);
			
			btnUpDown.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					urlIP = editIP.getText().toString();
					SendHttpRequestTask t = new SendHttpRequestTask();
					String fullURL = urlBegin + urlIP + urlEnd;
					String[] params = new String[]{fullURL};
					t.execute(params);
				}
			});
			
		}
		return rootView;
	}
	
	
	
	public class NutritionAdapter extends ArrayAdapter<String>
	{
		//private Context mContext;
		public NutritionAdapter(ArrayList<String> strings)
		{
			super(getActivity().getApplicationContext(), 0, strings);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent)
		{
			if(convertView == null)
			{
				convertView = getActivity().getLayoutInflater().inflate(R.layout.nutrition_list_item, null);
			}
			
			String s = getItem(position);
			TextView titleTextView = (TextView)convertView.findViewById(R.id.nutrition_list_item_title);
			titleTextView.setText(s);
			titleTextView.setTextAppearance(getContext(), R.style.button_text);
			
			//set listener for info button
			ImageButton infoButton = (ImageButton) convertView.findViewById(R.id.info_button);
			infoButton.setTag(position);
			infoButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//obtain position of list item to pass on to detailed view Activity
					int position = (Integer)v.getTag();
					Intent intent = new Intent((CollectionActivity)v.getContext(), NutritionActivity.class);
					intent.putExtra(NUT_ID, position);
					((CollectionActivity) v.getContext()).startActivity(intent);
				}
			});
			
			return convertView;
		}
	}	
	
	public class GraphViewData implements GraphViewDataInterface {
		private double x, y;

		public GraphViewData(double x, double y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public double getX() {
			return this.x;
		}

		@Override
		public double getY() {
			return this.y;
		}
	}
	
	private class SendHttpRequestTask extends AsyncTask<String, Void, String> {	
		@Override
		protected String doInBackground(String... params) {
			String url = params[0];
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			//read data into baos
			//InputStream in_s = getResources().openRawResource(R.raw.data2);
			String filename = BluetoothLeService.FILENAME;
			File file = new File(FileDir, filename);
			
			byte[] b_a;
			try {
				InputStream in_s = new FileInputStream(file);
				b_a = new byte[in_s.available()];
				in_s.read(b_a);
			    baos.write(b_a);
			    in_s.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//send baos
			try {
				HttpClient client = new HttpClient(url);
				client.connectForMultipart();
				client.addFilePart("file", "data2.txt", baos.toByteArray());
				client.finishMultipart();
				byte[] data = client.getResponse2();
				bmp = client.bmp;
				
			}
			catch(Throwable t) {
				t.printStackTrace();
			}
			
			return null;
		}

		@Override
		protected void onPostExecute(String data) {			
			//oitem.setActionView(null);
			resultView.setImageBitmap(bmp);
		}
	}

}
